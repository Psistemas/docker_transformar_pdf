from django import forms
from .models import Archivo
class Archivoform(forms.ModelForm):
    class Meta:
        model=Archivo
        fields="__all__"   
    def limpiar_archivo(self):
        archivo = self.cleaned_data.get('archivo')
        if archivo:
            if not archivo.name.endswith('.pdf'):
                raise forms.ValidationError('El archivo debe ser un PDF.')
        return archivo
        