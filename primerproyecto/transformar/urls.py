from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('',views.inicio,name="inicio"),
    path('buscar',views.buscar,name="buscar"),
    path("descargar/",views.descargar,name="descargar"),
    
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)