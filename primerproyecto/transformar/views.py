from django.shortcuts import render, redirect
from PyPDF2 import PdfReader
from .forms import Archivoform  # Asumiendo que tienes un formulario definido en "forms.py"
import base64
import io
import os
import subprocess
import time
import tabula
from tabula.io import read_pdf
import os       
from django.core.files.storage import default_storage
from django.conf import settings
from django.http import HttpResponse
from django.contrib import messages

def inicio(request):
    mensaje_exito = None  # Inicializar la variable del mensaje de éxito
    if request.method == 'POST':
        form = Archivoform(request.POST, request.FILES)
        if form.is_valid():
            messages.warning(request, "El archivo no tiene extension PDF.")
            pdf_file = request.FILES['archivo']
            if pdf_file.content_type != 'application/pdf':
                form.add_error('archivo', 'Solo se permiten archivos con extensión PDF')
            else:
                pdf_contenido = pdf_file.read()  # Leer contenido del archivo
                # Actualizar los datos en la sesión
                request.session['archivo_contenido'] = base64.b64encode(pdf_contenido).decode('utf-8')
                request.session['archivo_nombre'] = pdf_file.name
                request.session['archivo_subido'] = True
                return redirect('buscar')  
        else:
            messages.success(request, f"Archivo PDF '{pdf_file.name}' subido exitosamente.")
            mensaje_exito = "Buen trabajo"  # Mensaje de éxito 
    else:
        form = Archivoform()
    
    return render(request, 'paginas/inicio.html', {'form': form, 'mensaje_exito': mensaje_exito})

def buscar(request):
    archivo_subido = request.session.get('archivo_subido')
    archivo_nombre = None
    porcentaje_progreso = None
    tiempo_transcurrido = None
    tiempo_promedio_por_pagina = 5
    tiempo_estimado_total = None
    tiempo_inicial = time.time()
    if archivo_subido:
        archivo_nombre = request.session.get('archivo_nombre')
        archivo_contenido = base64.b64decode(request.session.get('archivo_contenido'))
        # Guardar el archivo subido para realizar el OCR
        input_folder_path = "/tmp"
        input_filename = os.path.join(input_folder_path, archivo_nombre)
        with open(input_filename, 'wb') as input_file:
            input_file.write(archivo_contenido)
        # Crear el nombre del archivo de salida OCR
        output_folder_path = "/tmp"
        output_filename = os.path.join(output_folder_path, "transformado_a_ocr_" + archivo_nombre)

        # Ejecutar OCR
        command = [
            "ocrmypdf",
            "--force-ocr",
            input_filename,
            output_filename
        ]
        
        try:
            result = subprocess.run(command, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        except subprocess.CalledProcessError as e:
            # Omitir el error y continuar
            print(e)
            pass
            

        tiempo_transcurrido = time.time() - tiempo_inicial
         # Utilizar Tabula para convertir PDF a CSV
    # Calcular el número de páginas en el PDF
    pdf_info = tabula.read_pdf(output_filename, pages='all')
    numero_de_paginas = len(pdf_info)
          
    csv_output_path = os.path.join(output_folder_path, "archivo_transformado.csv")
    tabula.io.convert_into(output_filename, csv_output_path, output_format="csv", pages='all')
    
    
    tiempo_transcurrido = time.time() - tiempo_inicial
    tiempo_estimado_total = tiempo_promedio_por_pagina * numero_de_paginas
    # Calcular el tiempo estimado total
    tiempo_estimado_por_pagina = tiempo_promedio_por_pagina  # Tiempo promedio por página
    tiempo_estimado_total = tiempo_estimado_por_pagina * numero_de_paginas  # Estimado total
    # Leer el CSV en un DataFrame
    try:
        df = tabula.read_pdf(csv_output_path, pages='all', header=None)
    except Exception as e:
        print(e)
        pass
    tiempo_transcurrido = time.time() - tiempo_inicial
        # tiempo
    return render(request, 'paginas/buscar.html', {
        'archivo_nombre': archivo_nombre,
        'porcentaje_progreso': porcentaje_progreso,
        'tiempo_transcurrido': tiempo_transcurrido,
        'tiempo_estimado_total': tiempo_estimado_total
    }) 
def descargar(request):
    archivo_csv = open("/tmp/archivo_transformado.csv", "rb")
    response = HttpResponse(archivo_csv, content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="archivo_transformado.csv"'
    return response





